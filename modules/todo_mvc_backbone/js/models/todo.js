/**
 * @file
 * A Backbone Model for the state of the in-place editing application.
 *
 * @see Drupal.quickedit.AppView
 */
Drupal.appBackbone = Drupal.appBackbone || {};
(function ($) {

  "use strict";

  Drupal.appBackbone.todo = Backbone.Model.extend({
    // Default attributes for the todo
    // and ensure that each todo created has `title` and `completed` keys.
    defaults: {
      title: '',
      completed: false,
      type: 'backbone',
    },
    initialize: function () {
      if (this.get('completed') ==0) this.set({'completed': false});
      else this.set({'completed' : true});
    },

    // Toggle the `completed` state of this todo item.
    toggle: function () {
      this.save({
        completed: !this.get('completed')
      });
    },
    //Add REST Settings
    url: function() {
      // Modified from Backbone.js to ignore collection and add ".format" extension.
      var base = Drupal.settings.basePath  + "json/todo/";
      if (this.isNew()) { return base; }
      // Add .json for format here.
      return base + this.id + ".json";
    },
  });
})(jQuery);
