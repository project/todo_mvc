/*global Backbone, jQuery, _, ENTER_KEY */
Drupal.appBackbone = Drupal.appBackbone || {};
(function ($) {
  'use strict';
  // The Application
  // ---------------
  // Our overall **AppView** is the top-level piece of UI.
  Drupal.appBackbone.AppView = Backbone.View.extend({

    // Instead of generating a new element, bind to the existing skeleton of
    // the App already present in the HTML.
    el: '#backbone-todoapp',

    // Our template for the line of statistics at the bottom of the Drupal.appBackbone
//    ,

    // Delegated events for creating new items, and clearing completed ones.
    events: {
      'keypress #backbone-new-todo': 'createOnEnter',
      'click #backbone-clear-completed': 'clearCompleted',
      'click #backbone-toggle-all': 'toggleAllComplete'
    },

    // At initialization we bind to the relevant events on the `Todos`
    // collection, when items are added or changed. Kick things off by
    // loading any preexisting todos that might be saved in *localStorage*.
    initialize: function () {
      this.statsTemplate =  _.template($('#backbone-stats-template').html());
      this.allCheckbox = this.$('#backbone-toggle-all')[0];
      this.$input = this.$('#backbone-new-todo');
      this.$footer = this.$('#backbone-footer');
      this.$main = this.$('#backbone-main');
      this.$list = $('#backbone-todo-list');

      this.listenTo(Drupal.appBackbone.todos, 'add', this.addOne);
      this.listenTo(Drupal.appBackbone.todos, 'reset', this.addAll);
      this.listenTo(Drupal.appBackbone.todos, 'change:completed', this.filterOne);
      this.listenTo(Drupal.appBackbone.todos, 'filter', this.filterAll);
      this.listenTo(Drupal.appBackbone.todos, 'all', _.debounce(this.render, 0));
      // Suppresses 'add' events with {reset: true} and prevents the app view
      // from being re-rendered for every model. Only renders when the 'reset'
      // event is triggered at the end of the fetch.
      Drupal.appBackbone.todos.fetch({reset: true});
       console.log(Drupal.appBackbone.todos.length);
    },

    // Re-rendering the App just means refreshing the statistics -- the rest
    // of the app doesn't change.
    render: function () {
      var completed = Drupal.appBackbone.todos.completed().length;
      var remaining = Drupal.appBackbone.todos.remaining().length;
      console.log(Drupal.appBackbone.todos.length);
      if (Drupal.appBackbone.todos.length) {
        this.$main.show();
        this.$footer.show();

        this.$footer.html(this.statsTemplate({
          completed: completed,
          remaining: remaining
        }));

        this.$('#filters li a')
          .removeClass('selected')
       //   .filter('[href="#/' + (Drupal.appBackbone.TodoFilter || '') + '"]')
          .addClass('selected');
      } else {
        this.$main.hide();
        this.$footer.hide();
      }

      this.allCheckbox.checked = !remaining;
    },

    // Add a single todo item to the list by creating a view for it, and
    // appending its element to the `<ul>`.
    addOne: function (todo) {
      var view = new Drupal.appBackbone.TodoView({ model: todo});
      this.$list.append(view.render().el);
    },

    // Add all items in the **Todos** collection at once.
    addAll: function () {
      this.$list.html('');
      Drupal.appBackbone.todos.each(this.addOne, this);
    },

    filterOne: function (todo) {
      todo.trigger('visible');
    },

    filterAll: function () {
      Drupal.appBackbone.todos.each(this.filterOne, this);
    },

    // Generate the attributes for a new Todo item.
    newAttributes: function () {
      return {
        title: this.$input.val().trim(),
        order: Drupal.appBackbone.todos.nextOrder(),
        completed: false
      };
    },

    // If you hit return in the main input field, create new **Todo** model,
    // persisting it to *localStorage*.
    createOnEnter: function (e) {
      if (e.which === ENTER_KEY && this.$input.val().trim()) {
        Drupal.appBackbone.todos.create(this.newAttributes());
        this.$input.val('');
      }
    },

    // Clear all completed todo items, destroying their models.
    clearCompleted: function () {
      _.invoke(Drupal.appBackbone.todos.completed(), 'destroy');
      return false;
    },

    toggleAllComplete: function () {
      var completed = this.allCheckbox.checked;

      Drupal.appBackbone.todos.each(function (todo) {
        todo.save({
          completed: completed
        });
      });
    }
  });
})(jQuery);
