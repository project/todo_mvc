var app = app || {};
var ENTER_KEY = 13;
var ESC_KEY = 27;
(function($) {

/**
 *
 */
Drupal.behaviors.backboneToDo = {
  attach: function (context, settings) {
      'use strict';
  // kick things off by creating the `App`

  new Drupal.appBackbone.AppView();
  Drupal.appBackbone.todos.url = Drupal.settings.basePath  + "/json/todo";

  }
};


})(jQuery);


