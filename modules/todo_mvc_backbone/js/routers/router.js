/*global Backbone */
Drupal.appBackbone = Drupal.appBackbone || {};

(function () {
  'use strict';

  // Todo Router
  // ----------
  var TodoRouter = Backbone.Router.extend({
    routes: {
      '*filter': 'setFilter'
    },

    setFilter: function (param) {
      // Set the current filter to be used
      Drupal.appBackbone.TodoFilter = param || '';

      // Trigger a collection filter event, causing hiding/unhiding
      // of Todo view items
      Drupal.appBackbone.todos.trigger('filter');
    }
  });

  Drupal.appBackbone.TodoRouter = new TodoRouter();
  Backbone.history.start();
})();
