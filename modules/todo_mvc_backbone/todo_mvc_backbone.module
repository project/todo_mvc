<?php
/**
 * Implements hook_library().
 */
function todo_mvc_backbone_library() {
  $libraries['twig.js'] = array(
    'title' => 'Twig.js',
    'website' => 'https://github.com/justjohn/twig.js',
    'version' => '0.4.3',
    'js' => array(
      libraries_get_path('twig.js') . '/twig.min.js' => array(),
    ),
  );
  $libraries['underscore'] = array(
    'title' => 'Underscore.js',
    'website' => 'http://documentcloud.github.com/underscore/',
    'version' => '1.3.1',
    'js' => array(
      libraries_get_path('underscore') . '/underscore-min.js' => array(),
    ),
  );
  $libraries['backbone'] = array(
    'title' => 'Backbone.js',
    'website' => 'http://documentcloud.github.com/backbone/',
    'version' => '0.9.1',
    'js' => array(
      libraries_get_path('backbone') . '/backbone-min.js' => array(),
    ),
    'dependencies' => array(
      array('todo_mvc_backbone' , 'underscore'),
    ),
  );
  return $libraries;
}

function todo_mvc_backbone_preprocess_block(&$variables) {
  if ($variables['block']->module == 'todo_mvc_backbone') {
    $variables['classes_array'][] = drupal_html_class('todo-mvc-block');
  }
}
/**
 * Implements hook_block_info().
 *
 * This hook declares what blocks are provided by the module.
 */
function todo_mvc_backbone_block_info() {
  $blocks['todo_backbone'] = array(
    'info' => t('ToDo Backbone'),
    'region' => 'content',
    'status' => TRUE,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 *
 * This hook generates the contents of the blocks themselves.
 */
function todo_mvc_backbone_block_view($delta = '') {
  $block = array();
  // The $delta parameter tells us which block is being requested.
  switch ($delta) {
    case 'todo_backbone':
      $block['subject'] = t('ToDo Backbone');
      $block['content'] = _todo_backbone_block();
    break;
  }
  return $block;
}

/**
 * A module-defined block content function.
 */
function _todo_backbone_block() {
  //Add Backbone JS & Underscore JS
  drupal_add_library('todo_mvc_backbone', 'backbone');

  //drupal_add_js('http://todomvc.com/examples/backbone/node_modules/todomvc-common/base.js', 'external');
  //drupal_add_js('http://todomvc.com/examples/backbone/node_modules/backbone.localstorage/backbone.localStorage.js', 'external');
  $content['#markup'] = '<section id="backbone-todoapp" class ="todoapp">
      <header id="backbone-header">
        <input id="backbone-new-todo" class="new-todo" placeholder="What needs to be done?" autofocus size="50">
      </header>
      <section id="backbone-main">
        <input id="backbone-toggle-all" type="checkbox" class="toggle-all">
        <label for="backbone-toggle-all"><label>
        <ul id="backbone-todo-list" class="todo-list"></ul>
      </section>
      <footer id="backbone-footer"></footer>
    </section>
    <footer id="backbone-info" class="todo-info">
      <p>Double-click to edit a todo</p>
      <p>Written by <a href="https://www.drupal.org/u/webankit">Ankit Babbar</a></p>
    </footer>
    <script type="text/template" id="backbone-item-template">
      <div class="view">
        <input class="toggle" type="checkbox" <%= completed ? \'checked\' : \'\' %>>
        <label><%- title %></label>
        <button class="destroy"></button>
      </div>
      <input class="edit" value="<%- title %>">
    </script>
    <script type="text/template" id="backbone-stats-template">
      <span id="backbone-todo-count" class="todo-count"><strong><%= remaining %></strong> <%= remaining === 1 ? \'item\' : \'items\' %> left</span>
      <ul id="backbone-filters" class="filters">
        <li><button class="selected"> All </button></li>
        <li><button> Active </button></li>
        <li><button> Completed </button></li>
      </ul>
      <% if (completed) { %>
      <button id="clear-completed">Clear completed</button>
      <% } %>
    </script>';
    $path = drupal_get_path('module', 'todo_mvc_backbone');
    $base_path = drupal_get_path('module', 'todo_mvc');
    $content['#attached'] = array(
      'js' => array(
         $path . '/js/localStorage.js',
         $path . '/js/app.js',
         $path . '/js/models/todo.js',
         $path . '/js/collections/todos.js',
         $path . '/js/views/app-view.js',
         $path . '/js/views/todo-view.js',
          $path . '/js/routers/router.js',
      ),
      'css' => array(
        $base_path . '/base.css',
       ),
    );
  return $content;
}
