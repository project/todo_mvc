<?php
/**
 * @file
 * todo_mvc_services_endpoint.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function todo_mvc_services_endpoint_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "services" && $api == "services") {
    return array("version" => "3");
  }
}
